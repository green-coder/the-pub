(ns the-pub.core
  (:require [integrant.core :as ig]
            [the-pub.http-server]
            [the-pub.ring-handler]))

(def config
  {:http-kit/server {:handler (ig/ref :ring/handler)
                     :port 3000}
   :ring/handler {}})

(def system
  (ig/init config))

(comment
  (ig/halt! system))
