(ns the-pub.ring-handler
  (:require [clojure.data.json :as json]
            [clojure.java.io :as io]
            [clojure.set :as set]
            [integrant.core :as ig]
            [ring.util.response :as response]
            [ring.middleware.params :refer [wrap-params]]
            [ring.middleware.session :refer [wrap-session]]
            [ring.middleware.session.memory :refer [memory-store]]
            [reitit.ring :as rr]
            [clj-http.client :as http-client]
            [hiccup.page :as hp]
            [buddy.core.hash :as hash]
            [buddy.core.dsa :as dsa])
  (:import (java.io StringWriter)
           (java.util Base64 Locale)
           (java.time ZoneId LocalDateTime)
           (java.time.format DateTimeFormatter)
           (java.security KeyPairGenerator SecureRandom)
           (org.bouncycastle.openssl PEMParser)
           (org.bouncycastle.util.io.pem PemWriter PemObject)))

;; ---------------
;;   Host config
;; ---------------

;; Our host's parameters
(def host-fqdn "liho.404.taipei")
(def host-port nil)
(def host-fqdn+port (str host-fqdn
                         (when (some? host-port)
                           (str ":" host-port))))
(def host-url (str "https://" host-fqdn+port "/"))

;; Our local test user
(def test-actor-id "romu")
(def test-activity-id (str "create-hello-world-" test-actor-id))
(def test-object-id (str "hello-world-" test-actor-id))

;; The host with which we are communicating.
(def target-host "pouet.chapril.org")
(def request-target "/inbox")


;; ------------------------
;;   URL helper functions
;; ------------------------

(defn get-activity-url [activity-id]
  (str host-url "activity/" activity-id))

(defn get-actor-url [actor-id]
  (str host-url "actor/" actor-id))

(defn get-actor-inbox-url [actor-id]
  (str (get-actor-url actor-id) "/inbox"))

(defn get-actor-outbox-url [actor-id]
  (str (get-actor-url actor-id) "/outbox"))

(defn get-actor-public-key-url [actor-id]
  (str (get-actor-url actor-id) "#main-key"))

(defn get-object-url [object-id]
  (str host-url "object/" object-id))


;; -------------------
;;   Message signing
;; -------------------

(defn http-date-now-str []
  (let [gmt-zone (ZoneId/of "GMT")
        date-time (LocalDateTime/now gmt-zone)
        formatter (-> (DateTimeFormatter/ofPattern "EEE, dd MMM yyyy HH:mm:ss z" Locale/ENGLISH)
                      (.withZone gmt-zone))]
    (-> date-time (.format formatter))))

(defn generate-keypair
  ([] (generate-keypair 2048))
  ([^long key-size]
   (let [keypair-generator (doto (KeyPairGenerator/getInstance "RSA" "BC")
                             (.initialize key-size (SecureRandom/getInstanceStrong)))
         keypair (.generateKeyPair keypair-generator)]
     {:public-key (.getPublic keypair)
      :private-key (.getPrivate keypair)})))

(defn decode64 [^String encoded-str]
  (-> (Base64/getDecoder) (.decode encoded-str)))

(defn encode64 [byte-array]
  (-> (Base64/getEncoder) (.encodeToString byte-array)))

(defn create-outgoing-request-signature [request-target digest target-host now-str actor]
  (let [signed-text (str "(request-target): post " request-target "\n"
                         "digest: " digest "\n"
                         "host: " target-host "\n"
                         "date: " now-str)
        signature (dsa/sign signed-text
                            {:key (:private-key actor)
                             :alg :rsassa-pkcs15+sha256 #_:rsassa-pss+sha256})]
    (str "keyId=\"" (get-actor-public-key-url (:id actor)) "\","
         "headers=\"(request-target) digest host date\","
         "signature=\"" (encode64 signature) "\"")))

;; ----------------
;;   toy database
;; ----------------

(defn public-key->pem-str [public-key]
  (let [pem-object (PemObject. PEMParser/TYPE_PUBLIC_KEY (.getEncoded public-key))
        str-writer (StringWriter.)]
    (with-open [pem-writer (PemWriter. str-writer)]
      (.writeObject pem-writer pem-object))
    (.toString str-writer)))

(defn create-person-actor [actor-id]
  (let [{:keys [public-key private-key]} (generate-keypair)]
    {:id   actor-id
     :type "Person"
     :public-key public-key
     :private-key private-key
     :publicKeyPem (public-key->pem-str public-key)}))

(def db
  {:activity {test-activity-id {:id     test-activity-id
                                :type   "Create"
                                :actor  test-actor-id
                                :object test-object-id}}
   :actor    {test-actor-id (create-person-actor test-actor-id)}
   :object   {test-object-id {:id           test-object-id
                              :type         "Note"
                              :published    "2022-01-16T00:00:00Z"
                              :attributedTo test-actor-id
                              :inReplyTo    "https://pouet.chapril.org/@greenCoder/107621156223633444"
                              :content      "<p>Hello, world! Message posted from my Clojure REPL ^_^ !!!</p>"
                              :to           "https://www.w3.org/ns/activitystreams#Public"}}})

;; ---------------------
;;   endpoint handlers
;; ---------------------

(defn webfinger-handler [request]
  (let [resource (get-in request [:params "resource"])
        [_ actor-id account-host] (re-matches #"acct:([^@]+)@(.+)" resource)]
    (if (or (nil? actor-id)
            (not= account-host host-fqdn+port)
            (not (contains? (:actor db) actor-id)))
      (response/not-found "Resource not found")
      {:status  200
       :headers {"Content-Type" "application/jrd+json; charset=utf-8"}
       :body    (-> {:subject resource
                     :links   [{:rel  "self"
                                :type "application/activity+json"
                                :href (get-actor-url actor-id)}]}
                    (json/write-str))})))

(defn nodeinfo-handler [request]
  {:status  200
   :headers {"Content-Type" "application/json; profile=\"http://nodeinfo.diaspora.software/ns/schema/2.1#\""}
   :body    (-> {:links [{:rel "http://nodeinfo.diaspora.software/ns/schema/2.1"
                          :href (str "https://" host-fqdn+port "/nodeinfo/2.1")}]}
                (json/write-str))})

(def nodeinfo-2_1-handler
  (constantly
    {:status  200
     :headers {"Content-Type" "application/json; profile=http://nodeinfo.diaspora.software/ns/schema/2.1#"}
     :body    (-> {:version "2.1"
                   :software {:name "the-pub"
                              :version "0.0.0-dev"
                              #_#_:repository ""
                              #_#_:homepage ""}
                   :protocols ["activitypub"]
                   :services {:inbound []
                              :outbound []}
                   :openRegistrations false
                   :usage {:users {:total 0
                                   :activeHalfyear 0
                                   :activeMonth 0}
                           :localPosts 0
                           :localComments 0}
                   :metadata {:nodeName "LiHo"}}
                  (json/write-str))}))

(defn get-activity-ld+json [activity-id]
  (-> (get-in db [:activity activity-id])
      (update :id get-activity-url)
      (update :actor get-actor-url)
      (update :object (fn [object-id]
                        (let [object (get-in db [:object object-id])]
                          (-> object
                              (update :id get-object-url)
                              (update :attributedTo get-actor-url)))))
      (assoc "@context" "https://www.w3.org/ns/activitystreams")
      (json/write-str)))

(defn activity-handler [request]
  (let [activity-id (get-in request [:path-params :activity-id])
        activity (get-in db [:activity activity-id])]
    (if (nil? activity)
      (response/not-found "Activity not found")
      {:status 200
       :headers {"Content-Type" "application/ld+json; profile=\"https://www.w3.org/ns/activitystreams\""}
       :body (get-activity-ld+json activity-id)})))

(defn actor-handler [request]
  (let [actor-id (get-in request [:path-params :actor-id])
        actor (get-in db [:actor actor-id])]
    (if (nil? actor)
      (response/not-found "Actor not found")
      {:status  200
       :headers {"Content-Type" "application/ld+json; profile=\"https://www.w3.org/ns/activitystreams\""}
       :body    (let [actor-url (get-actor-url actor-id)]
                   (-> {"@context"         ["https://www.w3.org/ns/activitystreams"
                                            "https://w3id.org/security/v1"]
                        :id                actor-url
                        :type              (:type actor)
                        :preferredUsername actor-id
                        :inbox             (get-actor-inbox-url actor-id)
                        ;;:outbox            (get-actor-outbox-url actor-id)
                        :publicKey         {:id           (get-actor-public-key-url actor-id)
                                            :owner        actor-url
                                            :publicKeyPem (:publicKeyPem actor)}}
                       (json/write-str)))})))

#_
(-> {:a 1, :b 2, :c "法國"}
    (json/write-str :escape-unicode false)
    (.getBytes "UTF-8")
    io/input-stream
    (io/reader :encoding "UTF-8")
    (json/read :key-fn keyword))

(defn actor-inbox-handler [request]
  (let [actor-id (get-in request [:path-params :actor-id])
        actor (get-in db [:actor actor-id])]
    (prn [:inbox (-> (:body request)
                     (io/reader :encoding "UTF-8")
                     (json/read :key-fn keyword)
                     (set/rename-keys {(keyword "@context") :_context}))])
    (if (nil? actor)
      (response/not-found "Actor not found")
      {:status 200
       :headers {"Content-Type" "text/plain"}
       :body "Ok"})))

(defn object-handler [request]
  (let [object-id (get-in request [:path-params :object-id])
        object (get-in db [:object object-id])]
    (if (nil? object)
      (response/not-found "Object not found")
      {:status 200
       :headers {"Content-Type" "application/ld+json; profile=\"https://www.w3.org/ns/activitystreams\""}
       :body (-> object
                 (update :id get-object-url)
                 (update :attributedTo get-actor-url)
                 (assoc "@context" "https://www.w3.org/ns/activitystreams")
                 (json/write-str))})))

(def not-found-handler
  (constantly
    (response/not-found
      (hp/html5
        [:head
         [:meta {:charset "UTF-8"}]]
        [:body
         [:h1 "Not found, sorry"]]))))

(defn logger [handler]
  (fn [request]
    (prn ((juxt :request-method :server-name :uri :query-string) request))
    (handler request)))

(def full-handler
  (rr/ring-handler
    (rr/router
      [["/.well-known/webfinger" {:get {:handler #'webfinger-handler
                                        :middleware [[wrap-params]]}}]
       ["/.well-known/nodeinfo" {:get {:handler #'nodeinfo-handler}}]
       ["/nodeinfo/2.1" {:get {:handler #'nodeinfo-2_1-handler}}]
       ["/activity/:activity-id" {:get {:handler #'activity-handler}}]
       ["/actor/:actor-id" {:get {:handler #'actor-handler}}]
       ["/actor/:actor-id/inbox" {:post {:handler #'actor-inbox-handler}}]
       ["/object/:object-id" {:get {:handler #'object-handler}}]])
    #'not-found-handler
    {:middleware [[logger]]}))

(comment
  ;; Reads the WebFinger information from a Mastodon instance.
  (-> (http-client/get "https://pouet.chapril.org/.well-known/webfinger"
                       {:query-params {"resource" "acct:greenCoder@pouet.chapril.org"}})
      :body
      (json/read-str :key-fn keyword))

  ;; Reads the user data from a Mastodon instance.
  (-> (http-client/get "https://pouet.chapril.org/users/greenCoder"
                       {:accept :json})
      :body
      (json/read-str :key-fn keyword))

  ;; Send a reply message to a Mastodon instance.
  (let [now-str     (http-date-now-str)
        actor       (get-in db [:actor test-actor-id])
        document    (get-activity-ld+json test-activity-id)
        body-digest (encode64 (hash/sha256 document))
        digest      (str "sha-256=" body-digest)]
    (http-client/post (str "https://" target-host request-target)
                      {:headers {;; "Content-Type" "application/ld+json; profile=\"https://www.w3.org/ns/activitystreams\""
                                 "Digest" digest
                                 "Host" target-host
                                 "Date" now-str
                                 "Signature" (create-outgoing-request-signature request-target digest target-host now-str actor)}
                       :body document}))

  (-> (http-client/get "https://pouet.chapril.org/users/greenCoder"
                       {:accept "application/activity+json"})
      :body
      (json/read-str {:key-fn keyword})
      (dissoc (keyword "@context")))

  (-> (http-client/get "https://pouet.chapril.org/.well-known/nodeinfo"
                       {:accept "application/json"}))

  (-> (http-client/get "https://pouet.chapril.org/nodeinfo/2.0"
                       {:accept "application/json"})
      :body
      (json/read-str {:key-fn keyword}))

  ,)

(defmethod ig/init-key :ring/handler [_ _]
  #'full-handler)
